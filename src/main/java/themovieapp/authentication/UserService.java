package themovieapp.authentication;

import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import themovieapp.shared.ResponseFormat;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	
	PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	
	public ResponseEntity<ResponseFormat<User>> login(UserCredentials userCredentials) {
		Optional<User> user = userRepository.findById(userCredentials.getEmail());

		if(user.isEmpty())
			return new ResponseEntity<ResponseFormat<User>>(new ResponseFormat<User>("User not found!", null), HttpStatus.UNAUTHORIZED);
		else if(!this.passwordEncoder.matches(userCredentials.getPassword(), user.get().getPassword()))
			return new ResponseEntity<ResponseFormat<User>>(new ResponseFormat<User>("Incorrect password!", null), HttpStatus.UNAUTHORIZED);
		else
			return new ResponseEntity<ResponseFormat<User>>(new ResponseFormat<User>("Logged in successfully!", user.get()), HttpStatus.OK);
	}
	
	public void register(User user){
		user.setPassword(this.passwordEncoder.encode(user.getPassword()));
		userRepository.save(user);	
	}
}
