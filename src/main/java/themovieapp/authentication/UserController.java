package themovieapp.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import themovieapp.shared.ResponseFormat;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/login")
	public ResponseEntity<ResponseFormat<User>> login(@RequestBody UserCredentials userCredentials) {
		return userService.login(userCredentials);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/register") 
	public void register(@RequestBody User user){
		userService.register(user);		
	}
}
